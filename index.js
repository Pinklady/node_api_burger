const express = require('express');
const models = require('./models');
const routes = require('./routes');
const app = express();

async function bootstrap() {
    await models.sequelize.authenticate().then(()=>{
        console.log('Connection has been established')
    })
        .catch(err =>{
            console.log('Unable to connect' , err)
        });
    await models.sequelize.sync().then(()=>{
        console.log('Sync')
    })
        .catch(err =>{
            console.log('Unable to connect' , err)
        });

    const app = express();
    routes(app);
    app.get('/', function (req, res) {
        res.send('Hello world!')
    });

    app.listen(process.env.PORT, function () {
        console.log(`Example app listening on port ${process.env.PORT}!`)
    });
}

bootstrap();





