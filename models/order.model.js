module.exports = function(sequelize, DataTypes) {
    const Order = sequelize.define('Order', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        daily_number: {
            type: DataTypes.BIGINT
        },
        date: {
            type: DataTypes.DATE
        },
        price: {
            type: DataTypes.DECIMAL,
            precision: 2
        }
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    Order.associate = (models) => {
        Order.belongsTo(models.User);
        Order.belongsToMany(models.Product,{through: 'ProductOrder', onDelete: 'cascade' , timestamps: false})
    };
    return Order;
};
