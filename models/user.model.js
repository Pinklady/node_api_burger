module.exports = function(sequelize, DataTypes) {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        firstname: {
            type: DataTypes.STRING
        },
        lastname: {
            type: DataTypes.STRING
        },
        role: {
            type: DataTypes.ENUM('ADMIN', 'CLIENT', 'PREP')
        }
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    User.associate = (models) => {
        User.hasMany(models.Session, { onDelete: 'cascade'});
        User.hasMany(models.Order, { onDelete: 'cascade' });
    };
    return User;
};
