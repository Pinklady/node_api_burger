module.exports = function(sequelize, DataTypes) {
    const Promotion = sequelize.define('Promotion', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        begin_date: {
            type: DataTypes.DATEONLY
        },
        end_date: {
            type: DataTypes.DATEONLY
        },
        rate: {
            type: DataTypes.DECIMAL,
            precision: 2
        }
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    Promotion.associate = (models) => {
        Promotion.belongsToMany(models.Product, {through: 'ProductPromotion', onDelete: 'cascade', timestamps: false});
    };
    return Promotion;
};
