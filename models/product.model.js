module.exports = function(sequelize, DataTypes) {
    const Product = sequelize.define('Product', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        type: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        },
        available: {
            type: DataTypes.BOOLEAN
        },
        begin_date: {
            type: DataTypes.DATEONLY
        },
        end_date: {
            type: DataTypes.DATEONLY
        },
        highlight: {
            type: DataTypes.BOOLEAN
        }
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    Product.associate = (models) => {
        Product.belongsToMany(models.Aliment, {through: 'ProductAliment', onDelete: 'cascade' , timestamps: false});
        Product.belongsToMany(models.Order, {through: 'ProductOrder', onDelete: 'cascade' , timestamps: false});
        Product.belongsToMany(Product, { as: 'SmallerProduct', through: 'ProductSmallerProduct', onDelete: 'cascade' , timestamps: false});
        Product.belongsToMany(models.Promotion, {through: 'ProductPromotion',  onDelete: 'cascade' , timestamps: false})
    };
    return Product;
};
