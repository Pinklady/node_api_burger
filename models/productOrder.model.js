module.exports = function(sequelize, DataTypes) {
    const ProductOrder = sequelize.define('ProductOrder', {
        quantity: DataTypes.INTEGER
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    return ProductOrder;
};
