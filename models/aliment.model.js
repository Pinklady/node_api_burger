module.exports = function(sequelize, DataTypes) {
    const Aliment = sequelize.define('Aliment', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        allergene: {
            type: DataTypes.BOOLEAN
        },
        stock: {
            type: DataTypes.INTEGER
        }
    }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
    });
    Aliment.associate = (models) => {
        Aliment.belongsToMany(models.Product, {through: 'ProductAliment', onDelete: 'cascade' , timestamps: false});
    };
    return Aliment;
};
