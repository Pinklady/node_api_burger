const models = require('../models');
const User = models.User;
const Session = models.Session;
const SecurityUtil = require('../utils').SecurityUtil;

class AuthController {

    /***
     *
     * @param email
     * @param password
     * @param firstname
     * @param lastname
     * @param role : 'ADMIN', 'CLIENT', 'PREP'
     * @returns {Promise<User>}
     */
    static subscribe(email, password, firstname, lastname, role) {
        return User.create({
            email,
            password: SecurityUtil.hashPassword(password),
            firstname,
            lastname,
            role
        });
    }

    /**
     * @param password
     * @returns {Promise<Session|null>}
     */
    static async login(email, password) {
        const user = await User.findOne({
            where: {
                email,
                password: SecurityUtil.hashPassword(password)
            }
        });
        if(!user) {
            return null;
        }
        const token = await SecurityUtil.randomToken();
        const session = await Session.create({
            token
        });
        await session.setUser(user);
        return session;
    }

    static userFromToken(token) {
        return User.findOne({
            include: [{
                model: Session,
                where: {
                    token
                }
            }]
        });
    }
}

module.exports = AuthController;
