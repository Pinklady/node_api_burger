const AuthMiddleware = require('../middlewares').AuthMiddleware;

module.exports = function(app) {

    app.get('/project', AuthMiddleware.auth(), async (req, res) => {
        console.log(req.user.id);
        res.status(204).end();
    });
};
